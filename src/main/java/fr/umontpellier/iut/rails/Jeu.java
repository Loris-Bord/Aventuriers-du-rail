package fr.umontpellier.iut.rails;

import com.google.gson.Gson;
import fr.umontpellier.iut.gui.GameServer;

import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.stream.Collectors;

public class Jeu implements Runnable {
    /**
     * Liste des joueurs
     */
    private List<Joueur> joueurs;

    /**
     * Le joueur dont c'est le tour
     */
    private Joueur joueurCourant;
    /**
     * Liste des villes représentées sur le plateau de jeu
     */
    private List<Ville> villes;
    /**
     * Liste des routes du plateau de jeu
     */
    private List<Route> routes;
    /**
     * Pile de pioche (face cachée)
     */
    private List<CouleurWagon> pileCartesWagon;
    /**
     * Cartes de la pioche face visible (normalement il y a 5 cartes face visible)
     */
    private List<CouleurWagon> cartesWagonVisibles;
    /**
     * Pile de cartes qui ont été défaussée au cours de la partie
     */
    private List<CouleurWagon> defausseCartesWagon;
    /**
     * Pile des cartes "Destination" (uniquement les destinations "courtes", les
     * destinations "longues" sont distribuées au début de la partie et ne peuvent
     * plus être piochées après)
     */
    private List<Destination> pileDestinations;
    /**
     * File d'attente des instructions recues par le serveur
     */
    private BlockingQueue<String> inputQueue;
    /**
     * Messages d'information du jeu
     */
    private List<String> log;

    public Jeu(String[] nomJoueurs) {


        // initialisation des entrées/sorties
        inputQueue = new LinkedBlockingQueue<>();
        log = new ArrayList<>();

        // création des cartes
        pileCartesWagon = new ArrayList<>();
        cartesWagonVisibles = new ArrayList<>();
        defausseCartesWagon = new ArrayList<>();
        pileDestinations = new ArrayList<>();

        this.initialisationPiles();

        // création des joueurs
        ArrayList<Joueur.Couleur> couleurs = new ArrayList<>(Arrays.asList(Joueur.Couleur.values()));
        Collections.shuffle(couleurs);
        joueurs = new ArrayList<>();
        for (String nom : nomJoueurs) {
            Joueur joueur = new Joueur(nom, this, couleurs.remove(0));
            joueurs.add(joueur);
        }
        joueurCourant = joueurs.get(0);

        // création des villes et des routes
        Plateau plateau = Plateau.makePlateauEurope();
        villes = plateau.getVilles();
        routes = plateau.getRoutes();
    }

    public void initialisationPiles(){
        this.ajouteNbExemplaireListeCarte(12);
        this.ajouteNbExemplaireListeLocomotive(14);
        this.ajouteCinqCarteWagonVisible();
        this.ajouteTouteLesDestinationsPile();
    }

    private void ajouteNbExemplaireListeCarte(int n){
        for(int i = 0; i < n; i++){
            this.pileCartesWagon.add(CouleurWagon.ROSE);
            this.pileCartesWagon.add(CouleurWagon.BLANC);
            this.pileCartesWagon.add(CouleurWagon.BLEU);
            this.pileCartesWagon.add(CouleurWagon.JAUNE);
            this.pileCartesWagon.add(CouleurWagon.ORANGE);
            this.pileCartesWagon.add(CouleurWagon.NOIR);
            this.pileCartesWagon.add(CouleurWagon.ROUGE);
            this.pileCartesWagon.add(CouleurWagon.VERT);
        }
    }

    private void ajouteCinqCarteWagonVisible(){
        for(int i = 0; i < 5; i++){
            this.cartesWagonVisibles.add(this.piocherCarteWagon());
        }
    }

    private void ajouteNbExemplaireListeLocomotive(int n){
        for(int i = 0; i < n; i++){
            this.pileCartesWagon.add(CouleurWagon.LOCOMOTIVE);
        }
    }

    private void ajouteTouteLesDestinationsPile(){
        this.pileDestinations.addAll(Destination.makeDestinationsEurope());
        this.pileDestinations.addAll(Destination.makeDestinationsLonguesEurope());
    }

    public Jeu(String[] nomJoueurs, ArrayList<Destination> listeDestination){
        this(nomJoueurs);
        this.pileDestinations.addAll(listeDestination);
    }

    public List<CouleurWagon> getPileCartesWagon() {
        return pileCartesWagon;
    }

    public List<CouleurWagon> getCartesWagonVisibles() {
        return cartesWagonVisibles;
    }

    public  List<CouleurWagon> getDefausseCartesWagon(){ return defausseCartesWagon;}

    public List<Ville> getVilles() {
        return villes;
    }

    public List<Route> getRoutes() {
        return routes;
    }

    public Joueur getJoueurCourant() {
        return joueurCourant;
    }

    public List<Destination> getPileDestinations() {return pileDestinations;}

    public void rajouteDestination(List<Destination> dest){this.pileDestinations.addAll(dest);}

    /**
     * Exécute la partie
     */
    public void run() {
        /*
         * ATTENTION : Cette méthode est à réécrire.
         * 
         * Cette méthode doit :
         * - faire choisir à chaque joueur les destinations initiales qu'il souhaite
         * garder : on pioche 3 destinations "courtes" et 1 destination "longue", puis
         * le
         * joueur peut choisir des destinations à défausser ou passer s'il ne veut plus
         * en défausser. Il doit en garder au moins 2.
         * - exécuter la boucle principale du jeu qui fait jouer le tour de chaque
         * joueur à tour de rôle jusqu'à ce qu'un des joueurs n'ait plus que 2 wagons ou
         * moins
         * - exécuter encore un dernier tour de jeu pour chaque joueur après
         */

        boolean finJeu = false;
        boolean tamponFinDeJeu = false;
        int numeroJoueur = 0;

        this.joueurs.forEach(joueur ->{
            int nbDestCourtes = 3;
            int nbDestLongues = 1;
            ArrayList<Destination> tampon = new ArrayList<>();
            ArrayList<Destination> courtes = Destination.makeDestinationsEurope();
            ArrayList<Destination> longues = Destination.makeDestinationsLonguesEurope();
            for (Destination d : this.pileDestinations) {

                if (longues.contains(d) && nbDestLongues > 0){
                    tampon.add(d);
                    nbDestLongues--;
                }else if (courtes.contains(d) && nbDestCourtes > 0){
                    tampon.add(d);
                    nbDestCourtes--;
                }else if (nbDestCourtes == 0 && nbDestLongues == 0){
                    break;
                }
            }
            tampon.removeAll(joueur.choisirDestinations(tampon,2));
            this.pileDestinations.removeAll(tampon);
            tampon.clear();
        });

        while (!finJeu){

            if (tamponFinDeJeu) finJeu = true;

            for (int i = 0; i < this.joueurs.size(); i++){

                this.joueurCourant = this.joueurs.get(numeroJoueur);

                this.joueurCourant.jouerTour(); //Le joueur joue son tour


                if (joueurCourant.getCartesWagon().size() <= 2){ //Si le joueur courant ne peut plus jouer alors...
                    tamponFinDeJeu = true; //La partie se terminera au prochain tour de table
                }

                if (numeroJoueur+1 > this.joueurs.size()){
                    numeroJoueur = 0;
                }else{
                    numeroJoueur++;
                }
            }
        }
    }

    /**
     * Ajoute une carte dans la pile de défausse.
     * Dans le cas peu probable, où il y a moins de 5 cartes wagon face visibles
     * (parce que la pioche
     * et la défausse sont vides), alors il faut immédiatement rendre cette carte
     * face visible.
     *
     * @param c carte à défausser
     */
    public void defausserCarteWagon(CouleurWagon c) {
        if (this.cartesWagonVisibles.size() < 5) this.cartesWagonVisibles.add(c);
        else this.defausseCartesWagon.add(c);
    }

    /**
     * Pioche une carte de la pile de pioche
     * Si la pile est vide, les cartes de la défausse sont replacées dans la pioche
     * puis mélangées avant de piocher une carte
     *
     * @return la carte qui a été piochée (ou null si aucune carte disponible)
     */
    public CouleurWagon piocherCarteWagon() {
        ArrayList<CouleurWagon> listeCW = new ArrayList<>();
        CouleurWagon cartePioche;
        if (this.pileCartesWagon.isEmpty()){
            if (this.defausseCartesWagon.isEmpty()){
                this.log("Il n'y a plus de cartes à piocher...");
                return null;
            }else{
                for (CouleurWagon wagon : this.defausseCartesWagon){
                    this.pileCartesWagon.add(wagon);
                    listeCW.add(wagon);
                }
                this.defausseCartesWagon.removeAll(listeCW);
                Collections.shuffle(this.pileCartesWagon); //Mélange la liste
            }
        }
        cartePioche = this.pileCartesWagon.get(0);
        this.pileCartesWagon.remove(cartePioche);
        return cartePioche;
    }

    /**
     * Retire une carte wagon de la pile des cartes wagon visibles.
     * Si une carte a été retirée, la pile de cartes wagons visibles est recomplétée
     * (remise à 5, éventuellement remélangée si 3 locomotives visibles)
     */
    public void retirerCarteWagonVisible(CouleurWagon c) {
        if(this.cartesWagonVisibles.size() > 0){
            this.cartesWagonVisibles.remove(c);
            for(int i = 0; i < (5-this.cartesWagonVisibles.size()); i++){
                this.cartesWagonVisibles.add(this.piocherCarteWagon());
            }
                int nbLoco = 0;
                for(CouleurWagon wagon : this.cartesWagonVisibles){
                    if(wagon.equals(CouleurWagon.LOCOMOTIVE)){
                        nbLoco++;
                    }
                }
                if(nbLoco == 3){
                    melangerCartesWagonVisible();
                }
        }

    }

    private void melangerCartesWagonVisible(){
        this.defausseCartesWagon.addAll(this.cartesWagonVisibles);
        this.cartesWagonVisibles.clear();
        for(int i = 0; i < 5; i++){
            CouleurWagon pioche = this.piocherCarteWagon();
            this.cartesWagonVisibles.add(pioche);
        }
    }

    public CouleurWagon choisirCarteWagonVisible(int i){
            CouleurWagon carteChoisie = this.cartesWagonVisibles.get(i);
            return carteChoisie;
    }

    /**
     * Pioche et renvoie la destination du dessus de la pile de destinations.
     * 
     * @return la destination qui a été piochée (ou `null` si aucune destination
     *         disponible)
     */
    public Destination piocherDestination() {
        if (this.pileDestinations.size() == 0){
            return null;
        }else{
            return this.pileDestinations.remove(0);
        }
    }

    public List<Joueur> getJoueurs() {
        return joueurs;
    }

    @Override
    public String toString() {
        StringJoiner joiner = new StringJoiner("\n");
        for (Joueur j : joueurs) {
            joiner.add(j.toString());
        }
        return joiner.toString();
    }

    /**
     * Ajoute un message au log du jeu
     */
    public void log(String message) {
        log.add(message);
    }

    /**
     * Ajoute un message à la file d'entrées
     */
    public void addInput(String message) {
        inputQueue.add(message);
    }

    /**
     * Lit une ligne de l'entrée standard
     * C'est cette méthode qui doit être appelée à chaque fois qu'on veut lire
     * l'entrée clavier de l'utilisateur (par exemple dans {@code Player.choisir})
     *
     * @return une chaîne de caractères correspondant à l'entrée suivante dans la
     *         file
     */
    public String lireLigne() {
        try {
            return inputQueue.take();
        } catch (InterruptedException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Envoie l'état de la partie pour affichage aux joueurs avant de faire un choix
     *
     * @param instruction l'instruction qui est donnée au joueur
     * @param boutons     labels des choix proposés s'il y en a
     * @param peutPasser  indique si le joueur peut passer sans faire de choix
     */
    public void prompt(String instruction, Collection<String> boutons, boolean peutPasser) {
        System.out.println();
        System.out.println(this);
        if (boutons.isEmpty()) {
            System.out.printf(">>> %s: %s <<<%n", joueurCourant.getNom(), instruction);
        } else {
            StringJoiner joiner = new StringJoiner(" / ");
            for (String bouton : boutons) {
                joiner.add(bouton);
            }
            System.out.printf(">>> %s: %s [%s] <<<%n", joueurCourant.getNom(), instruction, joiner);
        }

        Map<String, Object> data = Map.ofEntries(
                new AbstractMap.SimpleEntry<String, Object>("prompt", Map.ofEntries(
                        new AbstractMap.SimpleEntry<String, Object>("instruction", instruction),
                        new AbstractMap.SimpleEntry<String, Object>("boutons", boutons),
                        new AbstractMap.SimpleEntry<String, Object>("nomJoueurCourant", getJoueurCourant().getNom()),
                        new AbstractMap.SimpleEntry<String, Object>("peutPasser", peutPasser))),
                new AbstractMap.SimpleEntry<>("villes",
                        villes.stream().map(Ville::asPOJO).collect(Collectors.toList())),
                new AbstractMap.SimpleEntry<>("routes",
                        routes.stream().map(Route::asPOJO).collect(Collectors.toList())),
                new AbstractMap.SimpleEntry<String, Object>("joueurs",
                        joueurs.stream().map(Joueur::asPOJO).collect(Collectors.toList())),
                new AbstractMap.SimpleEntry<String, Object>("piles", Map.ofEntries(
                        new AbstractMap.SimpleEntry<String, Object>("pileCartesWagon", pileCartesWagon.size()),
                        new AbstractMap.SimpleEntry<String, Object>("pileDestinations", pileDestinations.size()),
                        new AbstractMap.SimpleEntry<String, Object>("defausseCartesWagon", defausseCartesWagon),
                        new AbstractMap.SimpleEntry<String, Object>("cartesWagonVisibles", cartesWagonVisibles))),
                new AbstractMap.SimpleEntry<String, Object>("log", log));
        GameServer.setEtatJeu(new Gson().toJson(data));
    }
}
