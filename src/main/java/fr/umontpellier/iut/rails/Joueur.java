package fr.umontpellier.iut.rails;


import java.util.*;
import java.util.stream.Collectors;

public class Joueur {

    /**
     * Les couleurs possibles pour les joueurs (pour l'interface graphique)
     */
    public static enum Couleur {
        JAUNE, ROUGE, BLEU, VERT, ROSE;
    }

    /**
     * Jeu auquel le joueur est rattaché
     */
    private Jeu jeu;
    /**
     * Nom du joueur
     */
    private String nom;
    /**
     * CouleurWagon du joueur (pour représentation sur le plateau)
     */
    private Couleur couleur;
    /**
     * Nombre de gares que le joueur peut encore poser sur le plateau
     */
    private int nbGares;
    /**
     * Nombre de wagons que le joueur peut encore poser sur le plateau
     */
    private int nbWagons;
    /**
     * Liste des missions à réaliser pendant la partie
     */
    private List<Destination> destinations;
    /**
     * Liste des cartes que le joueur a en main
     */
    private List<CouleurWagon> cartesWagon;
    /**
     * Liste temporaire de cartes wagon que le joueur est en train de jouer pour
     * payer la capture d'une route ou la construction d'une gare
     */
    private List<CouleurWagon> cartesWagonPosees;
    /**
     * Score courant du joueur (somme des valeurs des routes capturées)
     */
    private int score;


    public Joueur(String nom, Jeu jeu, Joueur.Couleur couleur) {
        this.nom = nom;
        this.jeu = jeu;
        this.couleur = couleur;
        nbGares = 3;
        nbWagons = 45;
        cartesWagon = new ArrayList<>();
        cartesWagonPosees = new ArrayList<>();
        destinations = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            this.cartesWagon.add(jeu.piocherCarteWagon());
        }
        score = 12; // chaque gare non utilisée vaut 4 points
    }

    public String getNom() {
        return nom;
    }

    public Couleur getCouleur() {
        return couleur;
    }

    public int getNbWagons() {
        return nbWagons;
    }

    public Jeu getJeu() {
        return jeu;
    }

    public int getNbGares() {return nbGares;}

    public int getScore(){return this.score;}

    public List<CouleurWagon> getCartesWagonPosees() {
        return cartesWagonPosees;
    }

    public List<CouleurWagon> getCartesWagon() {
        return cartesWagon;
    }

    public List<Destination> getDestinations() {
        return destinations;
    }

    /**
     * Attend une entrée de la part du joueur (au clavier ou sur la websocket) et
     * renvoie le choix du joueur.
     * <p>
     * Cette méthode lit les entrées du jeu ({@code Jeu.lireligne()}) jusqu'à ce
     * qu'un choix valide (un élément de {@code choix} ou de {@code boutons} ou
     * éventuellement la chaîne vide si l'utilisateur est autorisé à passer) soit
     * reçu.
     * Lorsqu'un choix valide est obtenu, il est renvoyé par la fonction.
     * <p>
     * Si l'ensemble des choix valides ({@code choix} + {@code boutons}) ne comporte
     * qu'un seul élément et que {@code canPass} est faux, l'unique choix valide est
     * automatiquement renvoyé sans lire l'entrée de l'utilisateur.
     * <p>
     * Si l'ensemble des choix est vide, la chaîne vide ("") est automatiquement
     * renvoyée par la méthode (indépendamment de la valeur de {@code canPass}).
     * <p>
     * Exemple d'utilisation pour demander à un joueur de répondre à une question
     * par "oui" ou "non" :
     * <p>
     * {@code
     * List<String> choix = Arrays.asList("Oui", "Non");
     * String input = choisir("Voulez vous faire ceci ?", choix, new ArrayList<>(), false);
     * }
     * <p>
     * <p>
     * Si par contre on voulait proposer les réponses à l'aide de boutons, on
     * pourrait utiliser :
     * <p>
     * {@code
     * List<String> boutons = Arrays.asList("1", "2", "3");
     * String input = choisir("Choisissez un nombre.", new ArrayList<>(), boutons, false);
     * }
     *
     * @param instruction message à afficher à l'écran pour indiquer au joueur la
     *                    nature du choix qui est attendu
     * @param choix       une collection de chaînes de caractères correspondant aux
     *                    choix valides attendus du joueur
     * @param boutons     une collection de chaînes de caractères correspondant aux
     *                    choix valides attendus du joueur qui doivent être
     *                    représentés par des boutons sur l'interface graphique.
     * @param peutPasser  booléen indiquant si le joueur a le droit de passer sans
     *                    faire de choix. S'il est autorisé à passer, c'est la
     *                    chaîne de caractères vide ("") qui signifie qu'il désire
     *                    passer.
     * @return le choix de l'utilisateur (un élément de {@code choix}, ou de
     * {@code boutons} ou la chaîne vide)
     */
    public String choisir(String instruction, Collection<String> choix, Collection<String> boutons,
                          boolean peutPasser) {
        // on retire les doublons de la liste des choix
        HashSet<String> choixDistincts = new HashSet<>();
        choixDistincts.addAll(choix);
        choixDistincts.addAll(boutons);

        // Aucun choix disponible
        if (choixDistincts.isEmpty()) {
            return "";
        } else {
            // Un seul choix possible (renvoyer cet unique élément)
            if (choixDistincts.size() == 1 && !peutPasser)
                return choixDistincts.iterator().next();
            else {
                String entree;
                // Lit l'entrée de l'utilisateur jusqu'à obtenir un choix valide
                while (true) {
                    jeu.prompt(instruction, boutons, peutPasser);
                    entree = jeu.lireLigne();
                    // si une réponse valide est obtenue, elle est renvoyée
                    if (choixDistincts.contains(entree) || (peutPasser && entree.equals("")))
                        return entree;
                }
            }
        }
    }

    /**
     * Affiche un message dans le log du jeu (visible sur l'interface graphique)
     *
     * @param message le message à afficher (peut contenir des balises html pour la
     *                mise en forme)
     */
    public void log(String message) {
        jeu.log(message);
    }

    @Override
    public String toString() {
        StringJoiner joiner = new StringJoiner("\n");
        joiner.add(String.format("=== %s (%d pts) ===", nom, score));
        joiner.add(String.format("  Gares: %d, Wagons: %d", nbGares, nbWagons));
        joiner.add("  Destinations: "
                + destinations.stream().map(Destination::toString).collect(Collectors.joining(", ")));
        joiner.add("  Cartes wagon: " + CouleurWagon.listToString(cartesWagon));
        return joiner.toString();
    }

    /**
     * @return une chaîne de caractères contenant le nom du joueur, avec des balises
     * HTML pour être mis en forme dans le log
     */
    public String toLog() {
        return String.format("<span class=\"joueur\">%s</span>", nom);
    }

    /**
     * Renvoie une représentation du joueur sous la forme d'un objet Java simple
     * (POJO)
     */
    public Object asPOJO() {
        HashMap<String, Object> data = new HashMap<>();
        data.put("nom", nom);
        data.put("couleur", couleur);
        data.put("score", score);
        data.put("nbGares", nbGares);
        data.put("nbWagons", nbWagons);
        data.put("estJoueurCourant", this == jeu.getJoueurCourant());
        data.put("destinations", destinations.stream().map(Destination::asPOJO).collect(Collectors.toList()));
        data.put("cartesWagon", cartesWagon.stream().sorted().map(CouleurWagon::name).collect(Collectors.toList()));
        data.put("cartesWagonPosees",
                cartesWagonPosees.stream().sorted().map(CouleurWagon::name).collect(Collectors.toList()));
        return data;
    }

    /**
     * Propose une liste de cartes destinations, parmi lesquelles le joueur doit en
     * garder un nombre minimum n.
     * <p>
     * Tant que le nombre de destinations proposées est strictement supérieur à n,
     * le joueur peut choisir une des destinations qu'il retire de la liste des
     * choix, ou passer (en renvoyant la chaîne de caractères vide).
     * <p>
     * Les destinations qui ne sont pas écartées sont ajoutées à la liste des
     * destinations du joueur. Les destinations écartées sont renvoyées par la
     * fonction.
     *
     * @param destinationsPossibles liste de destinations proposées parmi lesquelles
     *                              le joueur peut choisir d'en écarter certaines
     * @param n                     nombre minimum de destinations que le joueur
     *                              doit garder
     * @return liste des destinations qui n'ont pas été gardées par le joueur
     */
    public List<Destination> choisirDestinations(List<Destination> destinationsPossibles, int n) {

        List<Destination> destinationsNonGardees = new ArrayList<>();
        List<String> DestinationString = new ArrayList<>();

        while (destinationsPossibles.size() > n){

            destinationsPossibles.forEach(destination -> {
                this.jeu.log(destination.toString());
                DestinationString.add(destination.toString());

            });
            //choix pour le joueur//

            String choixJoueur = choisir("Choisissez une destination à retirer (passez sinon) : " + destinationsPossibles.toString(), DestinationString, new ArrayList<>(),true);
            //Le joueur choisit de passer, la boucle est stopée
            if(choixJoueur.equals("")){
                log("Aucune destination n'a été choisie");
                break;
            }else{
                ArrayList<Destination> destinationsDefausse = new ArrayList<>();
                destinationsPossibles.forEach(destination -> {
                    //On cherche la destination que le joueur a choisi parmis la liste
                    if (choixJoueur.equals(destination.toString())){
                        //On ajoute cette destination à la liste de celles défaussées et on la retire des destinations possibles
                        destinationsNonGardees.add(destination);
                        destinationsDefausse.add(destination);
                    }
                });
                destinationsPossibles.removeAll(destinationsDefausse);
                destinationsDefausse.clear();
            }
        }

        //Les destinations du joueurs sont celles qu'il a choisi de ne pas retirer (ajout des destinations possibles restantes dans la liste)
        this.destinations.addAll(destinationsPossibles);

        return destinationsNonGardees;
    }


    /**
     * Exécute un tour de jeu du joueur.
     * <p>
     * Cette méthode attend que le joueur choisisse une des options suivantes :
     * - le nom d'une carte wagon face visible à prendre ;
     * - le nom "GRIS" pour piocher une carte wagon face cachée s'il reste des
     * cartes à piocher dans la pile de pioche ou dans la pile de défausse ;
     * - la chaîne "destinations" pour piocher des cartes destination ;
     * - le nom d'une ville sur laquelle il peut construire une gare (ville non
     * prise par un autre joueur, le joueur a encore des gares en réserve et assez
     * de cartes wagon pour construire la gare) ;
     * - le nom d'une route que le joueur peut capturer (pas déjà capturée, assez de
     * wagons et assez de cartes wagon) ;
     * - la chaîne de caractères vide pour passer son tour
     * <p>
     * Lorsqu'un choix valide est reçu, l'action est exécutée (il est possible que
     * l'action nécessite d'autres choix de la part de l'utilisateur, comme "choisir les cartes wagon à défausser pour capturer une route" ou
     * "construire une gare", "choisir les destinations à défausser", etc.)
     */
    public void jouerTour() {
        ArrayList<String> choixPossible = new ArrayList<>();
        final String route = "class fr.umontpellier.iut.rails.Route";
        final String ferry = "class fr.umontpellier.iut.rails.Ferry";
        final String tunnel = "class fr.umontpellier.iut.rails.Tunnel";

        this.initChoixPossibles(choixPossible, false, false, false);

        boolean tourFini = false;
        int nbAction = 0;

        while (!tourFini) {
            String choixTour = this.choisir("Que voulez vous faire ?", choixPossible, new ArrayList<>(), true);
            if (this.choixType(choixTour, "COULEUR") != null) {
                switch (choixTour) {
                    case "GRIS" -> {
                        CouleurWagon choixCarte = this.jeu.piocherCarteWagon();
                        if (choixCarte != null){
                            this.cartesWagon.add(choixCarte);
                            nbAction++;
                        }
                    }
                    case "LOCOMOTIVE" -> {
                        this.cartesWagon.add(CouleurWagon.LOCOMOTIVE);
                        this.jeu.retirerCarteWagonVisible(CouleurWagon.LOCOMOTIVE);
                        nbAction += 2;
                    }
                    default -> {
                        this.cartesWagon.add(CouleurWagon.valueOf(choixTour));
                        this.jeu.retirerCarteWagonVisible(CouleurWagon.valueOf(choixTour));
                        nbAction++;
                    }
                }

            }else if(choixTour.equals("destinations")){
                ArrayList<Destination> listeD = new ArrayList<>(); //On initialise la liste des cartes destinations que l'on va piocher
                Destination dest;
                for (int i = 0; i < 3; i++) {
                    dest = this.jeu.piocherDestination();
                    if (dest != null) listeD.add(dest);
                    else {
                        log("Il n'y a plus de destinations dans la pile");
                        break;
                    }
                }
                this.jeu.rajouteDestination(this.choisirDestinations(listeD,2));
                nbAction += 2;

            }else if(this.choixType(choixTour, "VILLE") != null){
                if(this.nbGares > 0){ //Si le joueur peut encore poser des gares alors...
                    if(this.cartesWagon.size() >= 3-this.nbGares+1){ //Si le joueur possède au moins le nombre de cartes minimal pour poser sa gare alors...
                        joueCarteGares(3-this.nbGares+1);
                        this.nbGares--;
                        for(Ville ville : jeu.getVilles()){
                            if(ville.getNom().equals(choixTour)) {
                                ville.setProprietaire(this);
                                nbAction += 2; //La joueur à terminer son tour
                                this.score += 4;
                                break;
                            }
                        }
                    }
                }

            }else if (this.choixType(choixTour, "ROUTE") != null){
                ArrayList<String> couleursStringIdentiques = new ArrayList<>();
                Route route2 = null;
                for(Route route1 : jeu.getRoutes()){
                    if(route1.getNom().equals(choixTour)){
                        route2 = route1;
                        break;
                    }
                }
                
                switch (Objects.requireNonNull(route2).getClass().toString()){
                    case route -> {
                        if (route2.getCouleur() == CouleurWagon.GRIS) {
                            if (joueCarteCouleurGris(route2.getLongueur(), couleursStringIdentiques)) {
                                this.prendreRoute(route2);
                                nbAction += 2;
                            }
                        }else if (joueCarteCouleurNonGris(route2.getLongueur(), route2.getCouleur())){
                            this.prendreRoute(route2);
                            nbAction += 2;
                        }
                    }
                    case ferry -> {
                        Ferry ferry1 = (Ferry) route2;
                        if (joueCarteFerry(route2.getLongueur(), ferry1.getNbLocomotives(), couleursStringIdentiques)){
                            this.prendreRoute(route2);
                            nbAction += 2;
                        }
                    }
                    case tunnel -> {
                        if(joueCarteTunnel(route2.getLongueur(), couleursStringIdentiques)){
                            this.prendreRoute(route2);
                            nbAction += 2;
                        }else{
                            nbAction += 2;
                        }
                    }
                    default -> {

                    }

                }
            }
            if(nbAction == 2){
                tourFini = true;
            }

        }

    }

    private void prendreRoute(Route route){
        route.setProprietaire(this);
        System.out.println(this.cartesWagon.toString());
        this.ajoutScoreRoute(route.getLongueur());
    }

    private boolean joueCarteTunnel(int n, ArrayList<String> couleurStringIdentiques){
        ArrayList<String> piocheAlternative = new ArrayList<>();
        int locoDouble = 0;
        // On vérifie qu'on ne compte pas en double les locomotives
        for(CouleurWagon carte : this.cartesWagon){
            if(!carte.equals(CouleurWagon.LOCOMOTIVE)) locoDouble = Collections.frequency(this.cartesWagon, CouleurWagon.LOCOMOTIVE);
            else locoDouble = 0;
            if(Collections.frequency(this.cartesWagon, carte) + locoDouble >= n){
                this.cartesWagonPosees.add(carte);
            }
        }
        if(!this.cartesWagonPosees.isEmpty()){
            // On ajoute les locomotives restantes si il y en a
            for (int i = 0; i < Collections.frequency(this.cartesWagon, CouleurWagon.LOCOMOTIVE) - Collections.frequency(this.cartesWagonPosees, CouleurWagon.LOCOMOTIVE); i++){
               this.cartesWagonPosees.add(CouleurWagon.LOCOMOTIVE);
            }
        }
        this.cartesWagonPosees.forEach(couleurWagon -> couleurStringIdentiques.add(couleurWagon.name()));
        // Le joueur va choisir les cartes initiales qu'il veut joueur pour payer le tunnel (sans surcout)
        // Une fois la première couleure choisi, le joueur ne pourra que choisir cette meme couleur ou une locomotive pour la suite et rien d'autre
        if(!couleurStringIdentiques.isEmpty()) {
            this.cartesWagonPosees.clear();
            int j = 0;
            while (j < n) {
                String choix = this.choisir("Choisissez une carte à déposer :", couleurStringIdentiques, new ArrayList<>(), true);
                if (j == 0) {
                    // Le joueur choisit de passer on s'arrete
                    if (choix.equals("")) {
                        return false;
                        // Le joueur a initialement choisi une couleur, on retire les cartes qui ne sont soit pas le choix initial soit pas une locomotive
                    } else if (!choix.equals(CouleurWagon.LOCOMOTIVE.name())) {
                        couleurStringIdentiques.removeIf(carte -> !carte.equals(choix) & !carte.equals(CouleurWagon.LOCOMOTIVE.name()));
                        // coutSupplementaire = Collections.frequency(piocheAlternative, choix) + Collections.frequency(piocheAlternative, CouleurWagon.LOCOMOTIVE.name());
                    } else {
                        couleurStringIdentiques.removeIf(carte -> !carte.equals(CouleurWagon.LOCOMOTIVE.name()));
                        // coutSupplementaire = Collections.frequency(piocheAlternative, CouleurWagon.LOCOMOTIVE.name());
                    }
                }
                this.cartesWagonPosees.add(CouleurWagon.valueOf(choix));
                j++;
            }
            // On pioche trois carte et on les ajoute à la liste piocheAlternative pour pouvoir les comparer
            for (int i = 0; i < 3; i++) {
                piocheAlternative.add(jeu.piocherCarteWagon().name());
            }
            // On vérifie si le joueur a commencé à payer avec une locomotive, si c'est le cas on ne la comptera pas en double dans le décompte
            int nbLocoDouble = 0;
            int nbLocoDoublePioche = 0;
            if (!this.cartesWagonPosees.get(0).equals(CouleurWagon.LOCOMOTIVE)) {
                nbLocoDouble = Collections.frequency(this.cartesWagon, CouleurWagon.LOCOMOTIVE);
                nbLocoDoublePioche = Collections.frequency(piocheAlternative, CouleurWagon.LOCOMOTIVE.toString().toUpperCase());
            }
            // On vérifie que le nombre restant de carte de meme couleur du joueur est bien supérieur au nombre de cette meme carte dans la picoheAlternative
            // ( => Le joueur peut payer ou non)
            if ((Collections.frequency(this.cartesWagon, this.cartesWagonPosees.get(0)) + nbLocoDouble) - this.cartesWagonPosees.size() < Collections.frequency(piocheAlternative, this.cartesWagonPosees.get(0).toString().toUpperCase()) + nbLocoDoublePioche) {
                this.ajoutDefausseListeString((ArrayList<String>) piocheAlternative); // Les cartes piochées sont mises dans la défausse
                return false;
            } else {
                for (int k = 0; k < Collections.frequency(piocheAlternative, this.cartesWagonPosees.get(0).toString().toUpperCase()); k++) { // Le joueur choisit les cartes qu'il veut pour payer le cout supplémentaire du tunnel
                    String choix = this.choisir("Choisissez une carte à déposer :", couleurStringIdentiques, new ArrayList<>(), true);
                    // Le joueur choisit de ne pas continuer à prendre le tunnel
                    if (choix.equals("")) {
                        this.ajoutDefausseListeString(piocheAlternative); // On envoie à la défausse les cartes piochées
                        return false;
                    } else {
                        // On ajoute à la liste temporaire les couleurs que le joueur a choisi
                        this.cartesWagonPosees.add(CouleurWagon.valueOf(choix));
                    }
                }
            }
            this.poseTouteLesCartes(this.cartesWagonPosees); // Le joueur finit par payer le montant final tu tunnel
            this.ajoutDefausseListeString(piocheAlternative); // les cartes piochées sont défaussés
            return true;
        }
        return false;
    }

    private boolean joueCarteFerry(int n, int nbLoco, Collection<String> couleursStringIdentiques){
        final int nbCouleur = n-nbLoco;
        if (Collections.frequency(this.cartesWagon, CouleurWagon.LOCOMOTIVE) >= nbLoco ){ //Si le joueur à assez de locomotive dans sa main alors...
           this.cartesWagon.forEach(carte -> {
               if (Collections.frequency(this.cartesWagon,carte) >= nbCouleur || carte.equals(CouleurWagon.LOCOMOTIVE)){
                   this.cartesWagonPosees.add(carte);
               }
           });
           this.cartesWagonPosees.forEach(carte -> couleursStringIdentiques.add(carte.name()));

           while (n-nbLoco > 0 || nbLoco > 0){
               String choix = this.choisir("Choisissez une carte à déposer :", couleursStringIdentiques,new ArrayList<>(),false);
               if (!choix.equals(CouleurWagon.LOCOMOTIVE.name())) //Si le joueur ne choisis pas une locomotive alors...
                    couleursStringIdentiques.removeIf(carte -> !carte.equals(choix) & !carte.equals(CouleurWagon.LOCOMOTIVE.name()));
               this.poseCarte(1,choix);
               couleursStringIdentiques.remove(choix);
               if (CouleurWagon.valueOf(choix).equals(CouleurWagon.LOCOMOTIVE)){
                   n--;
                   nbLoco--;
               }else{
                   n--;
               }
           }
           this.cartesWagonPosees.clear();
           return true;
       }
        return false;
    }




    private boolean joueCarteCouleurGris(int n, Collection<String> couleursStringIdentiques){
        int locoDouble = 0;
        // On vérifie qu'on ne compte pas en double les locomotives
        for(CouleurWagon carte : this.cartesWagon){
            if(!carte.equals(CouleurWagon.LOCOMOTIVE)) locoDouble = Collections.frequency(this.cartesWagon, CouleurWagon.LOCOMOTIVE);
            else locoDouble = 0;
            if(Collections.frequency(this.cartesWagon, carte) + locoDouble >= n){
                this.cartesWagonPosees.add(carte);
            }
        }
        if(!this.cartesWagonPosees.isEmpty()){
            // On ajoute les locomotives restantes si il y en a
            for (int i = 0; i < Collections.frequency(this.cartesWagon, CouleurWagon.LOCOMOTIVE) - Collections.frequency(this.cartesWagonPosees, CouleurWagon.LOCOMOTIVE); i++){
                this.cartesWagonPosees.add(CouleurWagon.LOCOMOTIVE);
            }
        }
        this.cartesWagonPosees.forEach(couleurWagon -> couleursStringIdentiques.add(couleurWagon.name()));
        int j = 0;
        if(!couleursStringIdentiques.isEmpty()) {
            while (j < n) {
                String choix = this.choisir("Choisissez une carte à déposer :", couleursStringIdentiques, new ArrayList<>(), false);
                if (!choix.equals(CouleurWagon.LOCOMOTIVE.name())) {
                    couleursStringIdentiques.removeIf(carte -> !carte.equals(choix) & !carte.equals(CouleurWagon.LOCOMOTIVE.name()));
                }
                this.poseCarte(1, choix);
                j++;
            }
            this.cartesWagonPosees.clear();
            return true;
        }
        return false;
    }



    private boolean joueCarteCouleurNonGris(int n, CouleurWagon couleurRoute){
        Collection<String> couleursStringIdentiques = new ArrayList<>();
        if (Collections.frequency(this.cartesWagon, couleurRoute) + Collections.frequency(this.cartesWagon, CouleurWagon.LOCOMOTIVE) > n) {
            this.cartesWagon.forEach(carte -> {
                if (carte.equals(couleurRoute) || carte.equals(CouleurWagon.LOCOMOTIVE))
                    this.cartesWagonPosees.add(carte);
            });
            int i = 0;
            this.cartesWagonPosees.forEach(couleurWagon -> couleursStringIdentiques.add(couleurWagon.name()));
            while (i < n) {
                String choix = this.choisir("Choisissez une carte à poser :", couleursStringIdentiques, new ArrayList<>(), false);
                this.poseCarte(1,choix);
                i++;
            }
            this.cartesWagonPosees.clear();
            return true;
        }
        return false;
    }

    private boolean joueCarteGares(int n){

        Collection<CouleurWagon> couleursIdentiques = new ArrayList<>();
        Collection<String> couleursStringIdentiques = new ArrayList<>();

        for (CouleurWagon wagon : this.cartesWagon) {
            if (Collections.frequency(this.cartesWagon, wagon) >= n)
                if (!couleursIdentiques.contains(wagon)) couleursIdentiques.add(wagon);
        }
        if(!couleursIdentiques.isEmpty()) {
            couleursIdentiques.forEach(couleurWagon -> couleursStringIdentiques.add(couleurWagon.name()));
            String choix = this.choisir("Choisissez une couleur à défausser :", couleursStringIdentiques, new ArrayList<>(), false);
            this.poseCarte(n,choix);
            return true;
        }
        return false;
    }

    private void poseCarte(int nombre, String choix){
        for (int i = 0; i < nombre; i++) {
            this.cartesWagon.remove(CouleurWagon.valueOf(choix));
            this.jeu.getDefausseCartesWagon().add(CouleurWagon.valueOf(choix));
        }
    }

    private void initChoixPossibles(Collection<String> choix, boolean queCouleur, boolean queVille, boolean queRoute){
        if(!queVille && !queRoute) {
            this.jeu.getCartesWagonVisibles().forEach(c -> choix.add(c.name()));
            choix.add("GRIS");
        }
        if(!queCouleur && !queRoute){
            this.jeu.getVilles().forEach(ville -> {
                if (ville.getProprietaire() == null) choix.add(ville.getNom());
            });
        }
        if(!queCouleur && !queVille) {
            this.jeu.getRoutes().forEach(route -> {
                if (route.getProprietaire() == null)choix.add(route.getNom());
            });
        }

        if(!queCouleur && !queRoute && !queVille) {
            choix.add("destinations");
            choix.add("");
        }
    }

    public void test2(ArrayList<String> choix, boolean queCouleur, boolean queVille, boolean queRoute){
        this.initChoixPossibles(choix, queCouleur, queVille, queRoute);
        System.out.println(choix);
    }

    public String choixType(String choix, String choixType){
        ArrayList<String> liste = new ArrayList<>();
        switch (choixType){
            case "COULEUR" -> {
                this.initChoixPossibles(liste, true, false, false);
                if (liste.contains(choix)) return choix;
            }
            case "VILLE" -> {
                this.initChoixPossibles(liste, false, true, false);
                if(liste.contains(choix)) return choix;
            }
            case "ROUTE" -> {
                this.initChoixPossibles(liste, false, false, true);
                if(liste.contains(choix)) return choix;
            }
            default -> {
                return null;
            }
        }
        return null;
    }


    private void ajoutScoreRoute(Integer longueur){
        switch (longueur){
            case 1 -> {
                this.score += 1;
            }
            case 2 -> {
                this.score += 2;
            }
            case 3 -> {
                this.score += 4;
            }
            case 4 -> {
                this.score += 7;
            }
            case 6 -> {
                this.score += 15;
            }
            case 8 -> {
                this.score += 21;
            }
        }
    }
    private void ajoutDefausseListeString(ArrayList<String> liste){
        for(int i = 0; i < liste.size(); i++){
            this.jeu.getDefausseCartesWagon().add(CouleurWagon.valueOf(liste.get(i)));
        }
    }

    private void poseTouteLesCartes(Collection<CouleurWagon> cartesPosees){
        this.cartesWagon.removeAll(cartesPosees);
        this.jeu.getDefausseCartesWagon().addAll(cartesPosees);
    }


}
