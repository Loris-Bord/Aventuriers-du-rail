package fr.umontpellier.iut.rails;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class JeuTest {
    private Jeu jeu;
    private Joueur joueur1;
    private Joueur joueur2;
    private Joueur joueur3;
    private Joueur joueur4;


    @BeforeEach
    void init() {
        jeu = new Jeu(new String[] { "Guybrush", "Largo", "LeChuck", "Elaine" });
        List<Joueur> joueurs = jeu.getJoueurs();
        joueur1 = joueurs.get(0);
        joueur2 = joueurs.get(1);
        joueur3 = joueurs.get(2);
        joueur4 = joueurs.get(3);
    }


    @Test
    void test_Carte_a_defausser(){

        CouleurWagon g = CouleurWagon.GRIS;
        CouleurWagon bl = CouleurWagon.BLANC;

        jeu.defausserCarteWagon(g);

        assertEquals(g, jeu.getDefausseCartesWagon().get(0));
        assertEquals(5, jeu.getCartesWagonVisibles().size());

        assertEquals(1, jeu.getDefausseCartesWagon().size());
        assertEquals(5, jeu.getCartesWagonVisibles().size());
    }


    @Test
    void test_pioche_carte_wagon() {
        ArrayList<CouleurWagon> listeCW = new ArrayList<>();


        System.out.println(jeu.piocherCarteWagon().toLog());

        for (CouleurWagon cw : jeu.getPileCartesWagon()) {
            listeCW.add(cw);
        }
        jeu.getPileCartesWagon().removeAll(listeCW);
        assertEquals(0,jeu.getPileCartesWagon().size());
        assertEquals(0,jeu.getDefausseCartesWagon().size());

        assertEquals(null, jeu.piocherCarteWagon());
    }


    @Test
    void test_piocher_destination(){
        Jeu jeuDestination = new Jeu(new String[] { "Guybrush", "Largo", "LeChuck", "Elaine" }, Destination.makeDestinationsEurope());
        System.out.println(jeuDestination.piocherDestination());
    }


}
